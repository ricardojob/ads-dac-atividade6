package edu.ifpb.dac.dominio.mapped;

import javax.persistence.Entity;

/**
 *
 * @author Ricardo Job
 */
@Entity
public class Tecnico extends Funcionario {

    private String matricula;

    public Tecnico() {
    }

    public Tecnico(String matricula, String nome, String cpf) {
        super(nome, cpf);
        this.matricula = matricula;
    }

    
    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }
}
