/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ifpb.dac.dominio.mapped;

import javax.persistence.Entity;

/**
 *
 * @author Ricardo Job
 */
@Entity 
public class Analista extends Funcionario {

    private String matriculaAnalista;

    public Analista() {
    }

    public Analista(String matriculaAnalista, String nome, String cpf) {
        super(nome, cpf);
        this.matriculaAnalista = matriculaAnalista;
    }
    

    public String getMatriculaAnalista() {
        return matriculaAnalista;
    }

    public void setMatriculaAnalista(String matriculaAnalista) {
        this.matriculaAnalista = matriculaAnalista;
    }

}
