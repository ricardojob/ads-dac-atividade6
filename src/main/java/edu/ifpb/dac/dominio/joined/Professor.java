package edu.ifpb.dac.dominio.joined;

import javax.persistence.Entity;

/**
 *
 * @author Ricardo Job
 */
@Entity
public class Professor extends Pessoa {

    private String matriculaProf;

    public Professor() {
    }

    public Professor(String matriculaProf, String nome, String cpf) {
        super(nome, cpf);
        this.matriculaProf = matriculaProf;
    }

    
    public String getMatriculaProf() {
        return matriculaProf;
    }

    public void setMatriculaProf(String matriculaProf) {
        this.matriculaProf = matriculaProf;
    }
}
