package edu.ifpb.dac.dominio.joined;

import javax.persistence.Entity;

/**
 *
 * @author Ricardo Job
 */
@Entity
public class Aluno extends Pessoa {

    private String matricula;

    public Aluno() {
    }

    public Aluno(String matricula, String nome, String cpf) {
        super(nome, cpf);
        this.matricula = matricula;
    }

    
    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }
}
