 
package edu.ifpb.dac.dominio.table;

import javax.persistence.Entity;

/**
 *
 * @author Ricardo Job
 */
@Entity
public class Celta extends Carro {

    private String ano;

    public Celta() {
    }

    public Celta(String nome, String modelo, String ano) {
        super(nome, modelo);
        this.ano = ano;
    }

    public String getAno() {
        return ano;
    }

    public void setAno(String ano) {
        this.ano = ano;
    }

}
