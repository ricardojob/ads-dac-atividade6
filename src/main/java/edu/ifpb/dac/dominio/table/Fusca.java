package edu.ifpb.dac.dominio.table;

import javax.persistence.Entity;

/**
 *
 * @author Ricardo Job
 */
@Entity 
public class Fusca extends Carro {

    private String numeroDoChassi;

    public Fusca() {
    }

    public Fusca(String nome, String modelo,String numeroDoChassi ) {
        super(nome, modelo);
        this.numeroDoChassi = numeroDoChassi;
    }

    
    public String getNumeroDoChassi() {
        return numeroDoChassi;
    }

    public void setNumeroDoChassi(String numeroDoChassi) {
        this.numeroDoChassi = numeroDoChassi;
    }

    
}
