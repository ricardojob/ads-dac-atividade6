/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ifpb.dac.dominio.single;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 *
 * @author Ricardo Job
 */
@Entity
@DiscriminatorValue("Gato")
public class Gato extends Animal {

    private String manha;

    public Gato() {
    }

    public Gato(String nome, String raca, String manha) {
        super(nome, raca);
        this.manha = manha;
    }

    public String getManha() {
        return manha;
    }

    public void setManha(String manha) {
        this.manha = manha;
    }

}
