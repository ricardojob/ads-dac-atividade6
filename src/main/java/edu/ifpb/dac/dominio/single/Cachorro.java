package edu.ifpb.dac.dominio.single;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 *
 * @author Ricardo Job
 */
@Entity
@DiscriminatorValue("Cachorro")
public class Cachorro extends Animal {

    private String pedigree;

    public Cachorro() {
    }

    public Cachorro(String nome, String raca, String pedigree) {
        super(nome, raca);
        this.pedigree = pedigree;
    }

    public String getPedigree() {
        return pedigree;
    }

    public void setPedigree(String pedigree) {
        this.pedigree = pedigree;
    }

}
