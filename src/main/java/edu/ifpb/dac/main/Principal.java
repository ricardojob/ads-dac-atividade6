package edu.ifpb.dac.main;

import edu.ifpb.dac.dominio.joined.Aluno;
import edu.ifpb.dac.dominio.joined.Professor;
import edu.ifpb.dac.dominio.mapped.Analista;
import edu.ifpb.dac.dominio.mapped.Tecnico;
import edu.ifpb.dac.dominio.single.Cachorro;
import edu.ifpb.dac.dominio.single.Gato;
import edu.ifpb.dac.dominio.table.Celta;
import edu.ifpb.dac.dominio.table.Fusca;
import edu.ifpb.dac.infraestrutura.Dao;
import edu.ifpb.dac.infraestrutura.DaoJPA;

/**
 *
 * @author Ricardo Job
 */
public class Principal {

    public static void main(String[] args) {
        Dao dao = new DaoJPA("heranca");
        //Joined
        Aluno aluno = new Aluno("123", "Kiko", "369852");
        Professor prof = new Professor("325", "Girafales", "145236");
        dao.save(aluno);
        dao.save(prof);
        //MappedSuperClass
        Analista analista = new Analista("123", "Chaves", "214563");
        Tecnico tecnico = new Tecnico("325", "Nhonho", "987456");
        dao.save(analista);
        dao.save(tecnico);
        //Single_Table
        Cachorro cachorro = new Cachorro("Rex", "Lavrador", "Alto");
        Gato gato = new Gato("Fofinho", "Siamês", "Não é alto");
        dao.save(cachorro);
        dao.save(gato);
        //Table_Per_Class
        Fusca fusca = new Fusca("Querido 86", "76", "236541");
        Celta celta = new Celta("Potente", "Advanced", "2014");
        dao.save(fusca);
        dao.save(celta);
    }

}
